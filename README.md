# Document System

Document System is an application that allows you to manage different documents. It is avaliable two types of documents at our disposal: holiday application and questionnaire. The types of documents can change or expand over time.

## Libraries and tools

The program was written in IntelliJ IDEA. The project was converted to Maven project. It allows the easier management of external libraries and the compilation and building project.
External libraries: 

 - Lombok
 - commons-lang3
 - JUnit

## Running application

To start the application, use the following command: `mvn clean package` in main directory. Then use the following command: `java -jar target/BlueTestDocumentSystem-1.0-SNAPSHOT.jar`.


## Unit tests

Unit tests have been written to check the correct operation of the program.
