import org.junit.Test;

import static org.junit.Assert.assertEquals;

import pl.blueenergy.document.ApplicationForHolidays;
import pl.blueenergy.document.Question;
import pl.blueenergy.document.Questionnaire;
import pl.blueenergy.organization.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ProgrammerServiceTest {

    ProgrammerService programmerService = new ProgrammerService();

    @Test
    public void countAnswerAverage(){
        //given
        List<Questionnaire> questionnaires = new ArrayList<>();

        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setCreationDate(Calendar.getInstance().getTime());

        Question question = new Question();
        question.getPossibleAnswers().add("a1");
        question.getPossibleAnswers().add("a2");
        questionnaire.getQuestions().add(question);

        question = new Question();
        question.getPossibleAnswers().add("a1");
        question.getPossibleAnswers().add("a2");
        question.getPossibleAnswers().add("a3");
        question.getPossibleAnswers().add("a4");
        question.getPossibleAnswers().add("a5");
        question.getPossibleAnswers().add("a6");
        questionnaire.getQuestions().add(question);
        questionnaires.add(questionnaire);

        //when
        Double average = programmerService.countAnswerAverage(questionnaires);

        //then
        assertEquals(average, Double.valueOf(4.0));
    }

    @Test
    public void checkLatinCharset_shouldReturnEmptyList() {
        //given
        List<ApplicationForHolidays> applicationForHolidays = createValidApplicationForHolidays();

        //when
        List<User> users = programmerService.checkLatinCharset(applicationForHolidays);

        //then
        assertEquals(users.size(), 0);

    }

    @Test
    public void checkLatinCharset_shouldReturnNotEmptyList(){
        //given
        List<ApplicationForHolidays> applicationForHolidays = createValidApplicationForHolidays();
         applicationForHolidays.stream()
                .map(app -> app.getUserWhoRequestAboutHolidays())
                 .forEach(user -> user.setLogin("nówak32"));

        //when
        List<User> users = programmerService.checkLatinCharset(applicationForHolidays);

        //then
        assertEquals(users.size(), 1);
    }

    @Test
    public void validateDate_shouldNotValidate(){
        //given
        List<ApplicationForHolidays> applicationForHolidays = createValidApplicationForHolidays();

        //when
        List<ApplicationForHolidays> validatedHolidayDates = programmerService
                .validateHolidayDates(applicationForHolidays);

        //then
        assertEquals(validatedHolidayDates.size(), 0);

    }

    @Test
    public void validateDate_shouldValidate(){
        //given
        List<ApplicationForHolidays> applicationForHolidays = createValidApplicationForHolidays();

        applicationForHolidays.stream()
                .forEach(obj -> {
                    try {
                        obj.setSince(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-12"));
                        obj.setTo(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-07"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                });

        //when
        List<ApplicationForHolidays> validatedHolidayDates = programmerService
                .validateHolidayDates(applicationForHolidays);

        //then
        assertEquals(validatedHolidayDates.size(), 1);

    }


    private List<ApplicationForHolidays> createValidApplicationForHolidays(){
        User user = User.builder()
                .name("Jan")
                .surname("Nowak")
                .login("nowakjan12345")
                .build();

        ApplicationForHolidays applicationForHoliday = new ApplicationForHolidays();

        try {
            applicationForHoliday = ApplicationForHolidays.builder()
                    .since(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-12"))
                    .to((new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-20")))
                    .userWhoRequestAboutHolidays(user)
                    .build();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return Arrays.asList(applicationForHoliday);
    }
}
