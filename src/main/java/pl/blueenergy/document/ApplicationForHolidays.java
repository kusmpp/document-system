package pl.blueenergy.document;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.blueenergy.organization.User;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicationForHolidays extends Document{

	private Date since;
	private Date to;
	private User userWhoRequestAboutHolidays;
	
	public Date getSince() {
		return since;
	}
	public void setSince(Date since) {
		this.since = since;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
	public User getUserWhoRequestAboutHolidays() {
		return userWhoRequestAboutHolidays;
	}
	public void setUserWhoRequestAboutHolidays(User userWhoRequestAboutHolidays) {
		this.userWhoRequestAboutHolidays = userWhoRequestAboutHolidays;
	}
	
	
	
}
