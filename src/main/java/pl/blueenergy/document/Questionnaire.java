package pl.blueenergy.document;

import lombok.Builder;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

public class Questionnaire extends Document implements Serializable {

    private String title;
    private List<Question> questions = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        String text = getTitle() + "\n";
        for (Question question : getQuestions()) {
            text += question.getQuestionText() + "\n";
            for (int i = 0; i < question.getPossibleAnswers().size(); i++) {
                text += (i + 1) + ") " + question.getPossibleAnswers().get(i) + "\n";
            }
        }
        return text;
    }

}
