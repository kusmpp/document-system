package pl.blueenergy.utils;

import pl.blueenergy.document.Question;
import pl.blueenergy.document.Questionnaire;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class SaveToFileService {

    public static void saveQuestionnaireToFile(List<Questionnaire> questionnaires, String filePath) {

        List<String> questionnaireToWrite = questionnaires.stream()
                .map(questionnaire -> questionnaire.toString())
                .collect(Collectors.toList());
        Path path = Paths.get(filePath);
        try {
            Files.write(path, questionnaireToWrite, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
