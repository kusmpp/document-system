import pl.blueenergy.document.ApplicationForHolidays;
import pl.blueenergy.document.Document;
import pl.blueenergy.document.DocumentDao;
import pl.blueenergy.document.Questionnaire;
import pl.blueenergy.organization.User;
import pl.blueenergy.utils.SaveToFileService;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class ProgrammerService {

    public void execute(DocumentDao documentDao) {

        List<Document> allDocumentsInDatabase = documentDao.getAllDocumentsInDatabase();
        List<ApplicationForHolidays> applicationForHolidays = getApplicationForHolidays(allDocumentsInDatabase);
        List<Questionnaire> questionnaires = getQuestionnaire(allDocumentsInDatabase);

        Double questionsAverage = countAnswerAverage(questionnaires);

        List<User> usersWhoRequestedAboutHolidays = getAllHolidayRequestUsers(applicationForHolidays);

        List<User> usersWithNonLatinLoginCharacters = checkLatinCharset(applicationForHolidays);

        List<ApplicationForHolidays> applicationsWithIncorrectDates = validateHolidayDates(applicationForHolidays);

        SaveToFileService.saveQuestionnaireToFile(questionnaires, "questions.txt");

        changeSalaryFieldUsingReflection(applicationForHolidays.get(0).getUserWhoRequestAboutHolidays());

        System.out.println("Questions average: " + questionsAverage + "\n");
        System.out.println("Users who requested about holidays: " + usersWhoRequestedAboutHolidays + "\n");
        System.out.println("Users with problematic login: " + usersWithNonLatinLoginCharacters + "\n");
        System.out.println("Applications for holiday with invalid dates: " + applicationsWithIncorrectDates + "\n");
    }

    private List<ApplicationForHolidays> getApplicationForHolidays(List<Document> allDocumentsInDatabase) {

        return allDocumentsInDatabase.stream()
                .filter(obj -> obj instanceof ApplicationForHolidays)
                .map(obj -> (ApplicationForHolidays) obj)
                .collect(Collectors.toList());
    }

    private List<Questionnaire> getQuestionnaire(List<Document> allDocumentsInDatabase) {

        return allDocumentsInDatabase.stream()
                .filter(obj -> obj instanceof Questionnaire)
                .map(obj -> (Questionnaire) obj)
                .collect(Collectors.toList());
    }

    public Double countAnswerAverage(List<Questionnaire> questionnaires) {

        return questionnaires.stream()
                .map(Questionnaire::getQuestions)
                .flatMap(List::stream)
                .mapToInt(value -> value.getPossibleAnswers().size())
                .average()
                .getAsDouble();
    }

    private List<User> getAllHolidayRequestUsers(List<ApplicationForHolidays> applicationForHolidays) {

        return applicationForHolidays.stream()
                .map(ApplicationForHolidays::getUserWhoRequestAboutHolidays)
                .collect(Collectors.toList());
    }

    public List<User> checkLatinCharset(List<ApplicationForHolidays> applicationForHolidays) {

        return applicationForHolidays.stream()
                .map(ApplicationForHolidays::getUserWhoRequestAboutHolidays)
                .filter(user -> !user.getLogin().chars().allMatch(c -> c < 128))
                .collect(Collectors.toList());
    }

    public List<ApplicationForHolidays> validateHolidayDates(List<ApplicationForHolidays> applicationForHolidays) {

        return applicationForHolidays.stream()
                .filter(date -> date.getSince().after(date.getTo()))
                .collect(Collectors.toList());
    }

    private void changeSalaryFieldUsingReflection(User user) {

        try {
            Field field = user.getClass().getDeclaredField("salary");
            field.setAccessible(true);
            field.set(user, 25000);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
